package org.lab1.code.assignation;
import java.util.ArrayList;

import org.lab1.code.request.RequestList;
import org.lab1.code.room.Room;

public interface AssignationStrategy {
	
	public void reserveRoom(RequestList p_request, ArrayList<Room> p_roomList);
		

}
