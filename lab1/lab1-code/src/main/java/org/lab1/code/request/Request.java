package org.lab1.code.request;

import java.util.Comparator;

import org.lab1.code.room.Room;

public class Request implements Comparable<Request> {
	public static final Priority DEFAULT_PRIORITY = Priority.AVERAGE;
	public static final int DEFAULT_ID_REQUEST = 1;
	public static final int DEFAULT_NB_SEATS_NEEDED = 1;
	
	public enum Priority{
		VERY_HIGH(1), HIGH(2), AVERAGE(3), LOW(4), VERY_LOW(5);
		
		private final int value;

        private Priority(final int newValue) {
            value = newValue;
        }
        
        public static Priority fromValue(int value) {
            for (Priority priority: Priority.values()) {
                if (priority.value == value) {
                    return priority;
                }
            }
     
            return null;
        }

        public int getValue() { return value; }
	}
	
	private Room 	 assignedRoom;
	private int 	 id;
	private Priority priority;
	private int 	 nbSeatsNeeded;
	
	public Request() {
		this(DEFAULT_ID_REQUEST);
	}
	
	public Request(int nbSeatsNeeded) {
		this(DEFAULT_ID_REQUEST, DEFAULT_PRIORITY, nbSeatsNeeded);
	}
	
	public Request(Priority priority) {
		this(priority, DEFAULT_NB_SEATS_NEEDED);
	}
	
	public Request(Priority priority, int nbSeatsNeeded) {
		this(DEFAULT_ID_REQUEST, priority, nbSeatsNeeded);
	}
	
	public Request(int id, Priority priority, int nbSeatsNeeded) {
		this.setId(id);
		this.setPriority(priority);
		this.setNbSeatsNeeded(nbSeatsNeeded);
	}
	
	public void assignRoom(Room room) {
		this.assignedRoom = room;
	}
	
	public Room getAssignedRoom() {
		return assignedRoom;
	}

	@Override
	public int compareTo(Request request) {
		if(this.getPriority().getValue() < request.getPriority().getValue())
			return 1;
		else if(this.getPriority().getValue() > request.getPriority().getValue())
			return -1;
		
		return 0;
	}
	
	public Priority getPriority() {
		return priority;
	}

	public void setPriority(Priority priority) {
		this.priority = priority;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getNbSeatsNeeded() {
		return nbSeatsNeeded;
	}

	public void setNbSeatsNeeded(int nbSeatsNeeded) {
		this.nbSeatsNeeded = nbSeatsNeeded;
	}
	
	public String toString() {
		return this.getId() + " <" + this.getPriority().getValue() + "> s:" + this.getNbSeatsNeeded();
		
	}

	public static Comparator<Request> RequestPriorityComparator = new Comparator<Request>(){
		public int compare(Request request1, Request request2){
			return request2.compareTo(request1);
		}
	};
}
