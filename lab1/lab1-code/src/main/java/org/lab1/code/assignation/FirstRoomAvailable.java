package org.lab1.code.assignation;
import java.util.ArrayList;

import org.lab1.code.request.Request;
import org.lab1.code.request.RequestList;
import org.lab1.code.room.Room;

public class FirstRoomAvailable implements AssignationStrategy {

	@Override
	public void reserveRoom(RequestList p_request,
			ArrayList<Room> p_listRooms) {

		for (int i = 0; i < p_request.size(); i++) {
			assignRequestToFirstAvailableRoom(p_request.get(i), p_listRooms);
		}

	}

	private void assignRequestToFirstAvailableRoom(Request p_request,
			ArrayList<Room> p_listRooms) {

		Room firstAvailableRoom = getFirstRoomAvailable(p_listRooms);

		if (firstAvailableRoom.isAvailable()) {
			p_request.assignRoom(firstAvailableRoom);
			firstAvailableRoom.makeRoomUnavailable();
		}
	}

	private Room getFirstRoomAvailable(ArrayList<Room> p_listRooms) {
		Room firstRoom = new Room(); // if there is no available room, this one
										// will be returned
		firstRoom.makeRoomUnavailable(); // the reservation will fail because
											// the room is unavailable

		if (!p_listRooms.isEmpty()) {
			for (int i = 0; i < p_listRooms.size(); i++) {
				if (p_listRooms.get(i).isAvailable()) {
					firstRoom = p_listRooms.get(i);
					break;
				}
			}
		}

		return firstRoom;
	}

}
