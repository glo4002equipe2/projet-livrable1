package org.lab1.code;

public interface ScheduledRequestAssignmentService {
	 	
	public void StartSchedulerAssignment(Runnable assignRequestCommand);
	
	public void StopSchedulerAssignment();	

}
