package org.lab1.code.room;

import java.util.Comparator;

public class Room implements Comparable<Room> {
	public final static int DEFAULT_NUMBER_OF_SEATS = 1;
	public final static int DEFAULT_ROOM_ID = 1;
	public final static int MAX_SEATS_IN_ROOM = 200;
	
	private int 	totalSeats;
	private int 	id;
	private boolean isAvailable;
	
	public Room()
	{
		this(DEFAULT_ROOM_ID, DEFAULT_NUMBER_OF_SEATS);
	}
	
	public Room(int totalSeats)
	{
		this(DEFAULT_ROOM_ID, totalSeats);
	}
	
	public Room(int id, int totalSeats)
	{
		this.setId(id);
		this.totalSeats = totalSeats;
		this.isAvailable = true;
	}
	
	public int getNbSeats() {
		return totalSeats;
	}
	
	public boolean isAvailable() {
		return isAvailable;
	}

	public void makeRoomUnavailable() {
		isAvailable = false;		
	}

	public void makeRoomAvailable() {
		isAvailable = true;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String toString() {
		return this.getId() + " s:" + this.getNbSeats();
	}
	
	@Override
	public int compareTo(Room room) {
		if(this.getNbSeats() > room.getNbSeats())
			return 1;
		else if(this.getNbSeats() < room.getNbSeats())
			return -1;
		
		return 0;
	}

	public static Comparator<Room> RoomNbSeatsComparator = new Comparator<Room>(){
		public int compare(Room room1, Room room2){
			return room1.compareTo(room2);
		}
	};
}