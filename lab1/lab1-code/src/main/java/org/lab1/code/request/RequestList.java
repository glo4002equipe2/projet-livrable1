package org.lab1.code.request;

import java.util.ArrayList;
import java.util.stream.Collectors;

public class RequestList extends ArrayList<Request>{	

	public long countUnassigned(){
		return stream().filter((Request) -> Request.getAssignedRoom() == null).count();
	}
	
	public ArrayList<Request> getUnassigned(){
		return (ArrayList<Request>) stream().filter((request) -> (request.getAssignedRoom() == null)).collect(Collectors.toList());
	}
		
	public void sortByPriority() {
		sort(Request.RequestPriorityComparator);
	}
}