package org.lab1.code;


import java.util.ArrayList;

import org.lab1.code.request.Request;
import org.lab1.code.request.RequestList;
import org.lab1.code.assignation.AssignationStrategy;
import org.lab1.code.assignation.LowestNumberOfSeatsRequired;
import org.lab1.code.room.Room;

public class RequestManager {
	
	public static final int DEFAULT_ASSIGNMENT_REQUEST_THRESHOLD = 10;


	private RequestList listOfPendingRequests = new RequestList();
	private ArrayList<Room> listofEveryRoom;
	private AssignationStrategy usedAssignationStrategy;
	private ScheduledRequestAssignmentService scheduledRequestAssignmentService;
	private int assignmentRequestThreshold = DEFAULT_ASSIGNMENT_REQUEST_THRESHOLD;
	private static final LowestNumberOfSeatsRequired LowestNumberOfSeatsStrategyUsed = new LowestNumberOfSeatsRequired();


	private Runnable assignRequestCommand = new Runnable() {
	       public void run() {System.out.println("Assigning..."); assignRoomToTheListOfUnassignedRequest();}
	     };
	
	public RequestManager(ScheduledRequestAssignmentService scheduledRequestAssignmentService) {
		listOfPendingRequests = new RequestList();
		listofEveryRoom = new ArrayList<Room>();
		this.scheduledRequestAssignmentService = scheduledRequestAssignmentService;			
		scheduledRequestAssignmentService.StartSchedulerAssignment(assignRequestCommand);
		usedAssignationStrategy = LowestNumberOfSeatsStrategyUsed;
		
	}

	public int getAssignmentRequestThreshold(){
		return this.assignmentRequestThreshold;
	}
	

	public void setAssignmentRequestThreshold(int assignmentRequestThreshold){
		this.assignmentRequestThreshold = assignmentRequestThreshold;
	}

	public void addRequest(Request newRequest) {
		listOfPendingRequests.add(newRequest);

		if( listOfPendingRequests.countUnassigned() >= assignmentRequestThreshold) {
			
			scheduledRequestAssignmentService.StopSchedulerAssignment();
			assignRoomToTheListOfUnassignedRequest();
			scheduledRequestAssignmentService.StartSchedulerAssignment(assignRequestCommand);
		} 


		
	}

	public void addRoom(Room newRoom) {
		listofEveryRoom.add(newRoom);
	}

	public void assignRoomToTheListOfUnassignedRequest() {
		usedAssignationStrategy.reserveRoom(listOfPendingRequests,
				listofEveryRoom);

	}
	
	public void rearrangePendingRequestByPriorityOrder(){
		listOfPendingRequests.sortByPriority();
	}
	
	public void removeAssignedRequests() {

		for (int i = 0; i < listOfPendingRequests.size(); i++) {
			if (listOfPendingRequests.get(i).getAssignedRoom() != null) {
				listOfPendingRequests.remove(i);
				--i;
			}
		}

	}

	public ArrayList<Request> getListofPendingRequests() {
		return listOfPendingRequests;
	}

	public ArrayList<Room> getListRooms() {
		return listofEveryRoom;
	}

}
