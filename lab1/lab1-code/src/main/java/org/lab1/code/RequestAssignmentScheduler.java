package org.lab1.code;
import java.util.concurrent.*;

public class RequestAssignmentScheduler extends ScheduledThreadPoolExecutor implements ScheduledRequestAssignmentService  {
	

	public static final int DEFAULT_ASSIGNMENT_FREQUENCY_IN_MILLISECONDS = 60000;
	
	private int assignmentFrequencyInMilliseconds = DEFAULT_ASSIGNMENT_FREQUENCY_IN_MILLISECONDS;
	
	private ScheduledFuture<?> assignmentTask;
	
	private Runnable commandAssignment;
	
	public RequestAssignmentScheduler(int corePoolSize){
		super(corePoolSize);
	}
		
	public Runnable getCommandAssignment(){
		return commandAssignment;
	}
	
	public boolean isActive(){
		return assignmentTask != null;
	}
	
	public int getAssignmentFrequencyInMilliseconds(){
		return this.assignmentFrequencyInMilliseconds;
	}
	
	public void setAssignmentFrequencyInMilliseconds(int assignmentFrequency){
		this.assignmentFrequencyInMilliseconds = assignmentFrequency;
		
		if(this.isActive()){
			this.StopSchedulerAssignment();
			this.StartSchedulerAssignment(this.commandAssignment);
		}
	}

	@Override
	public void StartSchedulerAssignment(Runnable commandAssignment) {
		
		this.commandAssignment = commandAssignment;
		assignmentTask = this.scheduleWithFixedDelay(commandAssignment, assignmentFrequencyInMilliseconds, assignmentFrequencyInMilliseconds, TimeUnit.MILLISECONDS);
		
	}

	@Override
	public void StopSchedulerAssignment() {
		if(this.isActive()){
			if(!this.assignmentTask.isDone()){
				this.assignmentTask.cancel(false);
			}
			
			this.assignmentTask = null;	
		}		
	}

}
