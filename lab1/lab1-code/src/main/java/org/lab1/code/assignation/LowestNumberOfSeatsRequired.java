package org.lab1.code.assignation;
import java.util.ArrayList;

import org.lab1.code.request.Request;
import org.lab1.code.request.RequestList;
import org.lab1.code.room.Room;


public class LowestNumberOfSeatsRequired implements AssignationStrategy {

	@Override
	public void reserveRoom(RequestList p_request,
			ArrayList<Room> p_listRooms) {

		for (int i = 0; i < p_request.size(); i++) {
			assignRequestToRoomWithlowestNumberOfSeatsRequired(
					p_request.get(i), p_listRooms);
		}

	}

	private void assignRequestToRoomWithlowestNumberOfSeatsRequired(
			Request p_request, ArrayList<Room> p_listRooms) {

		Room firstAvailableRoom = getNextRoomAvailableWithRequiredSeats(
				p_request.getNbSeatsNeeded(), p_listRooms);

		if (firstAvailableRoom.isAvailable()) {
			p_request.assignRoom(firstAvailableRoom);
			firstAvailableRoom.makeRoomUnavailable();
		}
	}

	private Room getNextRoomAvailableWithRequiredSeats(int requiredSeats,
			ArrayList<Room> p_listRooms) {
		Room nextRoom = new Room(); // if there is no available room, this one
		// will be returned
		nextRoom.makeRoomUnavailable(); // the reservation will fail because
		// the room is unavailable

		int lowestNumberOfSeatsFound = Integer.MAX_VALUE;

		if (!p_listRooms.isEmpty()) {
			for (int i = 0; i < p_listRooms.size(); i++) {
				if (p_listRooms.get(i).isAvailable()
						&& p_listRooms.get(i).getNbSeats() >= requiredSeats
						&& p_listRooms.get(i).getNbSeats() < lowestNumberOfSeatsFound) {
					nextRoom = p_listRooms.get(i);
					lowestNumberOfSeatsFound = p_listRooms.get(i).getNbSeats();
				}
			}
		}

		return nextRoom;
	}

}
