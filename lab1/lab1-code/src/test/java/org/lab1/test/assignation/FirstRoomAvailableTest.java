package org.lab1.test.assignation;
import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;
import org.lab1.code.assignation.FirstRoomAvailable;
import org.lab1.code.request.Request;
import org.lab1.code.request.RequestList;
import org.lab1.code.room.Room;

public class FirstRoomAvailableTest {
	ArrayList<Room> listOfRooms;
	Room anAvailableRoom;
	Room anUnavailableRoom;

	FirstRoomAvailable firstRoomStrategy;

	RequestList simpleRequest;
	Request aRequest;

	@Before
	public void simpleSetup() {
		listOfRooms = new ArrayList<Room>();
		anAvailableRoom = new Room();
		anUnavailableRoom = new Room();
		anUnavailableRoom.makeRoomUnavailable();
		anUnavailableRoom.setId(2);

		firstRoomStrategy = new FirstRoomAvailable();

		aRequest = new Request();
		simpleRequest = new RequestList();
		simpleRequest.add(aRequest);

	}

	@Test
	public void simpleRequestAcceptedIfAvailableRoom() {
		listOfRooms.add(anAvailableRoom);

		firstRoomStrategy.reserveRoom(simpleRequest, listOfRooms);

		assertEquals(1, aRequest.getAssignedRoom().getId());
	}

	@Test
	public void simpleRequestDeniedIfOnlyUnavailableRoom() {
		listOfRooms.add(anUnavailableRoom);

		firstRoomStrategy.reserveRoom(simpleRequest, listOfRooms);

		assertEquals(null, aRequest.getAssignedRoom());

	}

	@Test
	public void simpleRequestAcceptedIfAvailableAndUnavailableRooms() {
		listOfRooms.add(anUnavailableRoom);
		listOfRooms.add(anAvailableRoom);

		firstRoomStrategy.reserveRoom(simpleRequest, listOfRooms);

		assertEquals(1, aRequest.getAssignedRoom().getId());
	}

	@Test
	public void simpleRequestDeniedIfEmptyRoomList() {

		firstRoomStrategy.reserveRoom(simpleRequest, listOfRooms);

		assertEquals(null, aRequest.getAssignedRoom());

	}

	@Test
	public void UsedRoomIsUnavailableWhenRequestIsAccepted() {
		listOfRooms.add(anAvailableRoom);

		firstRoomStrategy.reserveRoom(simpleRequest, listOfRooms);

		assertFalse(anAvailableRoom.isAvailable());
	}

	@Test
	public void MultipleRequestAreAssignedCorrectly() {
		Request aRequest2 = new Request();
		Request aRequest3 = new Request();
		Room anotherAvailableRoom = new Room();
		anotherAvailableRoom.setId(3);
		simpleRequest.add(aRequest2);
		simpleRequest.add(aRequest3);
		listOfRooms.add(anAvailableRoom);
		listOfRooms.add(anUnavailableRoom);
		listOfRooms.add(anotherAvailableRoom);

		firstRoomStrategy.reserveRoom(simpleRequest, listOfRooms);

		assertEquals(1, aRequest.getAssignedRoom().getId());
		assertEquals(3, aRequest2.getAssignedRoom().getId());
		assertEquals(null, aRequest3.getAssignedRoom());

	}

}
