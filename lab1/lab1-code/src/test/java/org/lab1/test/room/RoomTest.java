package org.lab1.test.room;

import static org.junit.Assert.*;

import org.junit.Test;
import org.lab1.code.request.Request;
import org.lab1.code.room.Room;

public class RoomTest {
	final int ONE = 1;
	@Test
	public void aNewRoomIsAvailable() {
		Room room = new Room();		
		assertTrue(room.isAvailable());
	}
	
	@Test
	public void aNewRoomHasNoSeats() {
		Room room = new Room();
		assertEquals(Room.DEFAULT_NUMBER_OF_SEATS, room.getNbSeats());
	}
	
	@Test
	public void aNewRoomHasDefaultId() {
		Room room = new Room();
		assertEquals(Request.DEFAULT_ID_REQUEST, room.getId());
	}
	
	@Test
	public void assigningARoomMakesItNotAvailable(){
		Room room = new Room();
		room.makeRoomUnavailable();
		assertFalse(room.isAvailable());
	}
	
	@Test
	public void checkingOutARoomMakesItAvailable(){
		Room room = new Room();
		room.makeRoomAvailable();;
		assertTrue(room.isAvailable());
	}
	
	@Test
	public void compareTwoRooms(){
		Room room1  = new Room(1,10);
		Room room2 = new Room(2,5);
		
		int result = room1.compareTo(room2);
		assertEquals(ONE,result);
	}
}