package org.lab1.test;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;
import org.lab1.code.RequestAssignmentScheduler;

public class RequestAssignmentSchedulerTest {
	private final int ASSIGNMENT_FREQUENCY_IN_MILLISECONDS = 2000;
	
	private RequestAssignmentScheduler requestAssignmentScheduler;
	private Runnable commandAssignement = new Runnable() {
	       public void run() {System.out.println("Assigning..."); }
	     };
	
	@Before
    public void setUp() {
		requestAssignmentScheduler = new RequestAssignmentScheduler(1);
    }
	
	@Test
	public void commandAssignmentNeverSettedIsNull(){		
		assertNull(requestAssignmentScheduler.getCommandAssignment());
	}

	
	@Test
	public void scheduleIsActive(){
		requestAssignmentScheduler.StartSchedulerAssignment(commandAssignement);
		
		assertTrue(requestAssignmentScheduler.isActive());
	}
	
	@Test
	public void scheduleIsNotActive(){
		requestAssignmentScheduler.StartSchedulerAssignment(commandAssignement);
		requestAssignmentScheduler.StopSchedulerAssignment();
		
		assertFalse(requestAssignmentScheduler.isActive());
	}
	
	@Test
	public void scheduleIsActiveAfterReactivation(){
		requestAssignmentScheduler.StartSchedulerAssignment(commandAssignement);
		requestAssignmentScheduler.StopSchedulerAssignment();
		requestAssignmentScheduler.StartSchedulerAssignment(commandAssignement);
		
		assertTrue(requestAssignmentScheduler.isActive());
	}
		
	@Test
	public void canConfigureAssignmentFrequencyWithoutScheduleStarted(){
		int assignmentFrequencyBefore =  requestAssignmentScheduler.getAssignmentFrequencyInMilliseconds();
		
		requestAssignmentScheduler.setAssignmentFrequencyInMilliseconds(RequestAssignmentScheduler.DEFAULT_ASSIGNMENT_FREQUENCY_IN_MILLISECONDS + ASSIGNMENT_FREQUENCY_IN_MILLISECONDS );		
		int assignmentFrequencyAfter = requestAssignmentScheduler.getAssignmentFrequencyInMilliseconds();
		
		assertNotEquals(assignmentFrequencyBefore, assignmentFrequencyAfter);		
	}
	
	@Test
	public void canConfigureAssignmentFrequencyWithScheduleStarted(){
		requestAssignmentScheduler.StartSchedulerAssignment(commandAssignement);		
		int assignmentFrequencyBefore =  requestAssignmentScheduler.getAssignmentFrequencyInMilliseconds();
		
		requestAssignmentScheduler.setAssignmentFrequencyInMilliseconds(RequestAssignmentScheduler.DEFAULT_ASSIGNMENT_FREQUENCY_IN_MILLISECONDS + ASSIGNMENT_FREQUENCY_IN_MILLISECONDS );		
		int assignmentFrequencyAfter = requestAssignmentScheduler.getAssignmentFrequencyInMilliseconds();
		
		assertNotEquals(assignmentFrequencyBefore, assignmentFrequencyAfter);		
	}

}
