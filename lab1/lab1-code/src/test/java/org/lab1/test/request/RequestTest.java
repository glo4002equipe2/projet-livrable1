package org.lab1.test.request;

import static org.junit.Assert.*;

import org.junit.Test;
import org.lab1.code.request.Request;
import org.lab1.code.request.Request.Priority;
import org.lab1.code.room.Room;

public class RequestTest {
	
	@Test
	public void assigningANewRoom() {
		Request request = new Request();
		Room room = new Room();
		
		request.assignRoom(room);
		
		assertEquals(request.getAssignedRoom(), room);
	}
	
	@Test
	public void aNewRequestHasDefaultPriority() {
		Request request = new Request();		
		assertEquals(Request.DEFAULT_PRIORITY, request.getPriority());
	}
	
	@Test
	public void aNewRequestHasDefaultId() {
		Request request = new Request();
		assertEquals(Request.DEFAULT_ID_REQUEST, request.getId());
	}
	
	@Test
	public void aNewRequestWithId() {
		Request request = new Request(Request.DEFAULT_ID_REQUEST);
		assertEquals(Request.DEFAULT_ID_REQUEST, request.getId());
	}
	
	@Test
	public void changingRequestPriorityOnInitialize() {
		Request request = new Request(Priority.VERY_LOW);		
		assertEquals(Priority.VERY_LOW, request.getPriority());
	}
	
	@Test
	public void changingRequestPriorityOnTheFly() {	
		Request request = new Request();		
		assertEquals(Request.DEFAULT_PRIORITY, request.getPriority());
		
		request.setPriority(Priority.VERY_LOW);		
		assertEquals(Priority.VERY_LOW, request.getPriority());
	}
	
	@Test
	public void comparingHigherPriorityToLowerOne() {
		Request request1 = new Request(Priority.HIGH);
		Request request2 = new Request(Priority.LOW);
		assertEquals(request1.compareTo(request2), 1);
	}
	
	@Test
	public void comparingEqualPriority() {
		Request request1 = new Request(Priority.AVERAGE);
		Request request2 = new Request(Priority.AVERAGE);
		assertEquals(request2.compareTo(request1), 0);
	}
}
