package org.lab1.test.request;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;
import org.lab1.code.request.Request;
import org.lab1.code.request.RequestList;


public class RequestListTest {
	final int NB_DEFAULT_REQUESTS = 3;
	
	
    RequestList requests;
	
	@Before
    public void setUp() {
        requests = new RequestList();
		
        requests.add(new Request(1));
        requests.add(new Request(2));
        requests.add(new Request(3));
    }
	
	@Test
	public void countUnassignedRequests() {
		assertEquals(NB_DEFAULT_REQUESTS, requests.countUnassigned());
	}
	

}
