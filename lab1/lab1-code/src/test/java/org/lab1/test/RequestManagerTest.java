
package org.lab1.test;

import static org.junit.Assert.*;


import static org.mockito.BDDMockito.*;


import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import org.lab1.code.RequestAssignmentScheduler;
import org.lab1.code.RequestManager;
import org.lab1.code.request.Request;
import org.lab1.code.request.Request.Priority;
import org.lab1.code.room.Room;

@RunWith(MockitoJUnitRunner.class)
public class RequestManagerTest {
	
	private final int NB_DEFAULT_ROOMS = 3;
	private final int NB_SEATS_50 = 50;
	private final int NB_SEATS_100 = 100;
	private final int NB_SEATS_150 = 150;
	private final int NB_SEATS_200 = 200;
	private final int ASSIGNMENT_REQUEST_THRESHOLD = 15;
	
	@Mock
	private RequestAssignmentScheduler mock_scheduler ;
	
	
	
	private RequestManager requestMng;
	
	@Before
    public void setUp() {
		willDoNothing().given(mock_scheduler).StartSchedulerAssignment(any());
		
        requestMng = new RequestManager(mock_scheduler);
        
        requestMng.addRoom(new Room(1));
        requestMng.addRoom(new Room(2));
        requestMng.addRoom(new Room(3));
    }
	
	@Test
	public void creatingANewRequestManagement() {
		requestMng = new RequestManager(mock_scheduler);
		assertTrue(requestMng.getListofPendingRequests().isEmpty());
		assertTrue(requestMng.getListRooms().isEmpty());
	}
		
	@Test
	public void assigningARequestToARoom(){
		Request newRequest = new Request();
		requestMng.addRequest(newRequest);
		
		requestMng.assignRoomToTheListOfUnassignedRequest();
		
		assertFalse(requestMng.getListRooms().get(0).isAvailable());
		assertNotNull(requestMng.getListofPendingRequests().get(0).getAssignedRoom());
	}
	
	@Test
	public void addingEmptyRequest() {
		requestMng.addRequest(new Request());

		assertNull(requestMng.getListofPendingRequests().get(0).getAssignedRoom());
	}
	
	@Test
	public void addingAvailableRoom() {
		requestMng.addRoom(new Room());

		assertTrue(requestMng.getListRooms().get(0).isAvailable());
	}	
	
	@Test 
	public void assigningAllRequestsToRoom() {
		requestMng.addRequest(new Request());
		requestMng.addRequest(new Request());
		requestMng.addRequest(new Request());
		
		requestMng.assignRoomToTheListOfUnassignedRequest();
		
		assertNotNull(requestMng.getListofPendingRequests().get(0).getAssignedRoom());
		assertNotNull(requestMng.getListofPendingRequests().get(1).getAssignedRoom());
		assertNotNull(requestMng.getListofPendingRequests().get(2).getAssignedRoom());
        
		assertFalse(requestMng.getListRooms().get(0).isAvailable());
		assertFalse(requestMng.getListRooms().get(1).isAvailable());
		assertFalse(requestMng.getListRooms().get(2).isAvailable());
	}
	
	@Test
	public void canConfigureAssignmentRequestThreshold(){
		int assignmentRequestThresholdBefore = requestMng.getAssignmentRequestThreshold();
		
		requestMng.setAssignmentRequestThreshold(RequestManager.DEFAULT_ASSIGNMENT_REQUEST_THRESHOLD +  ASSIGNMENT_REQUEST_THRESHOLD);
		int assignmentRequestThresholdAfter = requestMng.getAssignmentRequestThreshold();
		
		assertNotEquals(assignmentRequestThresholdBefore, assignmentRequestThresholdAfter);		
	}
		
	@Test
	public void addingRequestsOverThreshold() {
		for(int i = 0; i <= requestMng.getAssignmentRequestThreshold(); i++) { 
			requestMng.addRequest(new Request());
		}
		
        for(int i = 0; i <= requestMng.getAssignmentRequestThreshold(); i++) {
        	if(i < NB_DEFAULT_ROOMS) {
        		assertNotNull(requestMng.getListofPendingRequests().get(i).getAssignedRoom());
        	} else {
        		assertNull(requestMng.getListofPendingRequests().get(i).getAssignedRoom());
        	}
        }
		
        for(int i = 0; i < NB_DEFAULT_ROOMS; i++) {
        	assertFalse(requestMng.getListRooms().get(i).isAvailable());
        }
	}
	
	@Test
	public void requestsByAddOrder() {
		addingRequestsTestByPriority();
	}
	
	@Test
	public void assigningRequestsByPriority() {
		RequestManager requestMng2 = new RequestManager(mock_scheduler);
		requestMng2.addRoom(new Room(20));
		requestMng2.addRoom(new Room(20));
		requestMng2.addRequest(new Request(1, Priority.fromValue(1), Request.DEFAULT_NB_SEATS_NEEDED));
		requestMng2.addRequest(new Request(2, Priority.fromValue(2), Request.DEFAULT_NB_SEATS_NEEDED));
		requestMng2.addRequest(new Request(3, Priority.fromValue(3), Request.DEFAULT_NB_SEATS_NEEDED));
        
		requestMng2.rearrangePendingRequestByPriorityOrder();
        requestMng2.assignRoomToTheListOfUnassignedRequest();

        
        assertEquals(requestMng2.getListRooms().get(0), requestMng2.getListofPendingRequests().get(0).getAssignedRoom());
        assertEquals(requestMng2.getListRooms().get(1), requestMng2.getListofPendingRequests().get(1).getAssignedRoom());
        assertEquals(null,requestMng2.getListofPendingRequests().get(2).getAssignedRoom());
	}
	
	private void addingRequestsTestByPriority() {
		requestMng.addRequest(new Request(Priority.fromValue(1)));
		requestMng.addRequest(new Request(Priority.fromValue(2)));
		requestMng.addRequest(new Request(Priority.fromValue(3)));
		
		assertEquals(1, requestMng.getListofPendingRequests().get(0).getPriority().getValue());
		assertEquals(2, requestMng.getListofPendingRequests().get(1).getPriority().getValue());
		assertEquals(3, requestMng.getListofPendingRequests().get(2).getPriority().getValue());
	}
	

	@Test
	public void assigningRequestsByPriorityAndNbSeatsNeeded() {
		RequestManager requestMng2 = new RequestManager(mock_scheduler);
		requestMng2.addRoom(new Room(NB_SEATS_50));
		requestMng2.addRoom(new Room(NB_SEATS_100));
		requestMng2.addRoom(new Room(NB_SEATS_150));
		
    	requestMng2.addRequest(new Request(Priority.VERY_LOW, NB_SEATS_200)); 
    	requestMng2.addRequest(new Request(Priority.HIGH, NB_SEATS_150)); 
    	requestMng2.addRequest(new Request(Priority.AVERAGE, NB_SEATS_150)); 
    	requestMng2.addRequest(new Request(Priority.LOW, NB_SEATS_100)); 
    	requestMng2.addRequest(new Request(Priority.VERY_HIGH, NB_SEATS_100)); 
    	requestMng2.addRequest(new Request(Priority.AVERAGE, NB_SEATS_50)); 
    	
    	requestMng2.rearrangePendingRequestByPriorityOrder();
    	requestMng2.assignRoomToTheListOfUnassignedRequest();
        
    	assertEquals(requestMng2.getListRooms().get(1), requestMng2.getListofPendingRequests().get(0).getAssignedRoom());
    	assertEquals(requestMng2.getListRooms().get(2), requestMng2.getListofPendingRequests().get(1).getAssignedRoom());
    	assertNull(requestMng2.getListofPendingRequests().get(2).getAssignedRoom());
    	assertEquals(requestMng2.getListRooms().get(0), requestMng2.getListofPendingRequests().get(3).getAssignedRoom());
    	assertNull(requestMng2.getListofPendingRequests().get(4).getAssignedRoom());
    	assertNull(requestMng2.getListofPendingRequests().get(5).getAssignedRoom());
	}
}