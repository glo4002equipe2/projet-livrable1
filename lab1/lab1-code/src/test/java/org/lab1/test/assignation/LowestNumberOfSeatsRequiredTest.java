package org.lab1.test.assignation;
import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;
import org.lab1.code.assignation.LowestNumberOfSeatsRequired;
import org.lab1.code.request.Request;
import org.lab1.code.request.RequestList;
import org.lab1.code.room.Room;


public class LowestNumberOfSeatsRequiredTest {

	ArrayList<Room> listOfRooms;
	Room anAvailableRoomWithTenSeats;
	Room anotherAvailableRoomWithTwentySeats;
	Room anUnAvailableRoomWithTenSeats;

	LowestNumberOfSeatsRequired lowestNumberOfSeatsStrategy;

	RequestList simpleRequest;
	Request aRequestOfTenSeats;
	Request aRequestOfFifteenSeats;

	@Before
	public void simpleSetup() {
		listOfRooms = new ArrayList<Room>();
		
		anAvailableRoomWithTenSeats = new Room(10);//default ID of 1
		
		anUnAvailableRoomWithTenSeats = new Room(10);
		anUnAvailableRoomWithTenSeats.makeRoomUnavailable();
		anUnAvailableRoomWithTenSeats.setId(2);
		
		anotherAvailableRoomWithTwentySeats = new Room(20);
		anotherAvailableRoomWithTwentySeats.setId(3);
		

		lowestNumberOfSeatsStrategy = new LowestNumberOfSeatsRequired();

		aRequestOfTenSeats = new Request();
		aRequestOfTenSeats.setNbSeatsNeeded(10);
		aRequestOfFifteenSeats = new Request();
		aRequestOfFifteenSeats.setNbSeatsNeeded(15);
		simpleRequest = new RequestList();

	}

	@Test
	public void simpleRequestAcceptedIfAvailableRoomWithEnoughSeats() {
		listOfRooms.add(anAvailableRoomWithTenSeats);
		simpleRequest.add(aRequestOfTenSeats);

		lowestNumberOfSeatsStrategy.reserveRoom(simpleRequest, listOfRooms);

		assertEquals(1, aRequestOfTenSeats.getAssignedRoom().getId());
	}

	@Test
	public void simpleRequestDeniedIfOnlyUnavailableRoom() {
		listOfRooms.add(anUnAvailableRoomWithTenSeats);
		simpleRequest.add(aRequestOfTenSeats);

		lowestNumberOfSeatsStrategy.reserveRoom(simpleRequest, listOfRooms);

		assertEquals(null, aRequestOfTenSeats.getAssignedRoom());

	}
	
	@Test
	public void simpleRequestDeniedIfOnlyRoomWithoutEnoughSeats() {
		listOfRooms.add(anAvailableRoomWithTenSeats);
		simpleRequest.add(aRequestOfFifteenSeats);

		lowestNumberOfSeatsStrategy.reserveRoom(simpleRequest, listOfRooms);

		assertEquals(null, aRequestOfFifteenSeats.getAssignedRoom());

	}

	@Test
	public void simpleRequestAcceptedIfRoomsWithEnoughAndNotEnoughSeats() {
		listOfRooms.add(anAvailableRoomWithTenSeats);
		listOfRooms.add(anotherAvailableRoomWithTwentySeats);
		simpleRequest.add(aRequestOfFifteenSeats);

		lowestNumberOfSeatsStrategy.reserveRoom(simpleRequest, listOfRooms);

		assertEquals(3, aRequestOfFifteenSeats.getAssignedRoom().getId());
	}

	@Test
	public void simpleRequestDeniedIfEmptyRoomList() {
		simpleRequest.add(aRequestOfTenSeats);

		lowestNumberOfSeatsStrategy.reserveRoom(simpleRequest, listOfRooms);

		assertEquals(null, aRequestOfTenSeats.getAssignedRoom());

	}

	@Test
	public void TheRightRoomIsUnavailableWhenRequestIsAccepted() {
		listOfRooms.add(anAvailableRoomWithTenSeats);
		listOfRooms.add(anotherAvailableRoomWithTwentySeats);
		simpleRequest.add(aRequestOfFifteenSeats);

		lowestNumberOfSeatsStrategy.reserveRoom(simpleRequest, listOfRooms);

		assertTrue(anAvailableRoomWithTenSeats.isAvailable());
		assertFalse(anotherAvailableRoomWithTwentySeats.isAvailable());
		
	}

	@Test
	public void MultipleRequestAreAssignedCorrectly() {
		Request aSecondRequestofTenSeats = new Request();
		aSecondRequestofTenSeats.setNbSeatsNeeded(10);
		Request aThirdRequestofFifteenSeats = new Request();
		aThirdRequestofFifteenSeats.setNbSeatsNeeded(15);
		Request aFourthRequestofThirtySeats = new Request();
		aFourthRequestofThirtySeats.setNbSeatsNeeded(30);
		simpleRequest.add(aRequestOfTenSeats);
		simpleRequest.add(aSecondRequestofTenSeats);
		simpleRequest.add(aThirdRequestofFifteenSeats);
		simpleRequest.add(aFourthRequestofThirtySeats);
		Room anotherAvailableRoomWithTenSeats = new Room(10);
		anotherAvailableRoomWithTenSeats.setId(4);
		Room anotherAvailableRoomWithOneSeats = new Room(1);
		anotherAvailableRoomWithOneSeats.setId(5);
		listOfRooms.add(anAvailableRoomWithTenSeats);
		listOfRooms.add(anotherAvailableRoomWithOneSeats);
		listOfRooms.add(anotherAvailableRoomWithTenSeats);
		listOfRooms.add(anotherAvailableRoomWithTwentySeats);

		lowestNumberOfSeatsStrategy.reserveRoom(simpleRequest, listOfRooms);

		assertEquals(1, aRequestOfTenSeats.getAssignedRoom().getId());
		assertEquals(4, aSecondRequestofTenSeats.getAssignedRoom().getId());
		assertEquals(3, aThirdRequestofFifteenSeats.getAssignedRoom().getId());
		assertEquals(null, aFourthRequestofThirtySeats.getAssignedRoom());
		assertFalse(anotherAvailableRoomWithTwentySeats.isAvailable());
		assertFalse(anAvailableRoomWithTenSeats.isAvailable());
		assertFalse(anotherAvailableRoomWithTenSeats.isAvailable());
		assertTrue(anotherAvailableRoomWithOneSeats.isAvailable());


	}


}
